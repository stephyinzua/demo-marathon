// https://jsonplaceholder.typicode.com/users
// https://dummyimage.com/200x200/024983/ffffff&text=website

 
var requestOptions = {
        method : 'GET',
        redirect: 'follow'
};
fetch('https://jsonplaceholder.typicode.com/users', requestOptions)
.then( response => response.json())
.then(
    (data) => {
        let tabla =  document.getElementById("tabla");
        let html = '';
        for (const datum of data){
            html += (' <article class="cards__item">' +
           '<div class="cards__item__img">' +
            ' <img src="" alt="">'  +
            '</div>'  +

            '<h1 class="cards__item__titulo">'  +
                 datum.username +
            '</h1>' +
            '<div class="cards__item__dates">' +
            ' <div class="col__one">' + 
                    ' <p>Name: </p>' +
               '</div>' +
                '<div class="col__two">' +
            ' <p> ' + datum.name + '</p>' +
            '</div>' +
            '</div>' +
            '<div class="cards__item__dates">' +
            '<div class="col__one">' +
            ' <p>E-mail: </p>' +
            '</div>' +
            '<div class="col__two">' +
            '<p>' + datum.email + '</p>' +
            '</div>' +
            '</div>' +

            '<div class="cards__item__dates">' +
            '<div class="col__one">' +
                    '<p>Conpany: </p>' +
                '</div>' +
                '<div class="col__two">' +
                ' <p>' + datum.company.name + '</p>' +
               ' </div>' +
            '</div>' +

           ' <div class="cards__item__dates">' +
            ' <div class="col__one">' +
                ' <p>City: </p>' +
                '</div>' +
                '<div class="col__two">' +
                '<p> ' + datum.address.city + '</p>' +
                '</div>' +
           ' </div>' +
        '</article>'  )
        }
        tabla.innerHTML = html
        
      //  console.log(data)
    }).catch( error => console.log(error))

/*
 var requestOptionsTwo = {
        method : 'GET',
        redirect: 'follow'
};
fetch('https://dummyimage.com/200x200/024983/ffffff&text', requestOptionsTwo).then( response => response.text())
.then(
    (data) => {
        console.log(data)
    }).catch( error => console.log(error))*/